const LocalStrategy = require("passport-local");
const client = require("./databasepg");
const bcrypt = require("bcrypt");

//custom field setting
const customizeFields = {
  usernameField: "email", //setting req name that will hold values of input field.should match with front end side keys
  passwordField: "password",
};
//call backfunctions
const Strategy = async (username, password, done) => {
  try {
    //if no error try this
    const userFind = await client.query("SELECT * FROM users WHERE email =$1", [username]);
    if (userFind.rows.length <= 0) {
      return done(null, false, { message: "user do not exist with this email" });
    } else {
      if (userFind.rows.length > 0) {
        //if user exist with that email will compare password with hashed password from database
        bcrypt.compare(password, userFind.rows[0].password, (err, matehed) => {
          return done(null, userFind.rows[0]); //if password matched this will return user data
        });
      } else {
        //if password do not matched this will return message saying wrong password
        return done(null, false, { message: "wrong password" });
      }
    }
  } catch (err) {
    //if  error try this
    return done(err);
  }
};


//local Strategy initializing function
function initialize(passport) {
  passport.use(new LocalStrategy(customizeFields, Strategy));

  //serializing user attaching sessions to this user using id
  passport.serializeUser((user, done) => done(null, user.user_id));

  //fetching seassion from that user using id if
  passport.deserializeUser((id, done) => {
    client.query("SELECT * FROM users WHERE user_id = $1", [parseInt(id)], (err, results) => {
      console.log(id);
      if (err) {
        throw err;
      }
      //if not error return user
      return done(null, results.rows[0]);
    });
  });
}


module.exports=initialize;